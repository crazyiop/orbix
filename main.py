#!/usr/bin/env python
# -*- coding: utf-8 -*-

from machine import deepsleep, Pin

# from machine import unique_id

from random import seed
import gc

from orbixpixel import OrbixPixel
from mpu6050 import Accelero
from button import Buttons

from game import read_savegame
from wemos_games import Actions, WemosOrbix, WemosMap, WemosTwisty, WemosLuminations


_LED_DI_PIN = 2
_SELF_HOLDING = 15
_BUTTON_PINS = [0, 12, 14, 16]

# to commonize wemos and pc code
class FileNotFoundError(Exception):
    pass


def main():
    leds = OrbixPixel(_LED_DI_PIN)
    leds.expand_wave("Purple")

    # Avoid the button press to trigger a reset once powered on
    hold = Pin(_SELF_HOLDING, Pin.OUT)
    hold.on()

    # Depends on the wiring to the charlieplexing modules
    mappings = {
        "id1": {
            (0, 1): 0,
            (0, 2): 1,
            (0, 3): 2,
            (1, 0): 3,
            (1, 2): 4,
            (1, 3): 5,
            (2, 0): 6,
            (2, 1): 7,
            (2, 3): 8,
            (3, 0): 9,
            (3, 1): 10,
            (3, 2): 11,
        }
    }

    buttons = Button(_BUTTON_PINS, mappings["id1"])
    acc = Accelero()
    random_seed = acc.get_rand()
    print("random seed:", random_seed)
    seed(random_seed)

    constructors = [WemosOrbix, WemosTwisty, WemosLuminations, WemosMap]
    try:
        game_start, level_start, state = read_savegame()
    except (OSError, FileNotFoundError):
        game_start, level_start, state = 0, 1, None

    while True:
        for i in range(game_start, len(constructors)):
            game_start = 0

            gc.collect()  # reclaim old game used
            game = constructors[i](leds, buttons)

            level = level_start - 1
            level_start = 1
            while True:
                level += 1
                action = game.play(level, state)
                state = None

                if action == Actions["NEXT_GAME"]:
                    break
                elif action == Actions["QUIT"]:
                    print("Goodbye !")
                    with open("savegame.sav", "w") as save:
                        save.write(str(i) + "\n")
                        save.write(str(level) + "\n")
                        save.write(str(game.state))
                    hold.off()
                    leds.fade_out()
                    deepsleep()
                elif action == Actions["NEXT_LEVEL"]:
                    pass
                if action == "win":
                    pass
