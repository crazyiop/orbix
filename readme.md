# Orbix refurbishing
This project aim to refurbished an old non-working [orbix](https://www.twistypuzzles.com/cgi-bin/puzzle.cgi?pkey=901) I had as a child. This would not be really fun if more level challenge were not added in the process :)

## Electronics
### Components
The electronics will be replaced with:
 - a [wemos mini like](https://wiki.wemos.cc/products:d1:d1_mini) microcontroller running [micropython](https://micropython.org/) for the brain.
 - RGB LEDs (ws2812/[neopixel](https://www.adafruit.com/categories/168)).
 - I don't think an accelerometer is needed, but I have a working prototype with a [mpu6050](https://www.invensense.com/products/motion-tracking/6-axis/mpu-6050/) breadboard.
 - normaly closed opto-relay for the automatic power off (see section below)
 - a small lipo battery and the charging circuit (by far the most expensive of the list)

All those components can be found quite cheap on ebay.

### PCB
I made 3 pcb for this project:

 - Main pcb: A pcb that fit most components and the wemos.
 - lED pcb: 12 small pcb that replace the 12 ones for eachbutton/LED. Mine need 6 connectors (gnd, vcc, di, do, row, colm) where the old one needed 3 (common gnd, button, LED). That's because I use chained RGB lEDs, and because I need to charlieplexed the button given the number of pin I have.
 - Charlieplexing pcb: 4 pcb that do the charlieplexing wiring and allow to not concentrate all button wire into one single point.

### Buttons
The Buttons are placed in a charlieplexing array. There is some precaution to take by chosing the pins for the rows as a pull up is needed and some pins have hardware pull up/down on the mini board.

The scan function should be able to differentiate one-button press from multiple button press, in order to be able to navigate the games and the level. In the original orbix, pressing together two buttons next to each other (dist=1) cycle through the level, and two buttons of dist=2 deactivate the sound (or vice versa).

### Automatic power off, boot from existing button
The puzzle should power itself off automatically after a long enough period of inactivity. This is easy to do. But should be able to be awaken from an existing button (adding anything will looks terrible). The problem is then: As a battery powered project, I want the micro to be really off, then I need to use the reste pin to awak him. How to connect one existing button to the reset pin, without having a reset occuring every time this button is used for the puzzle solving.

My solution is to use 2 [CPC1117N](http://www.ixysic.com/home/pdfs.nsf/www/CPC1117N.pdf/$file/CPC1117N.pdf) components, which are *normally closed* opto-relay. When off, they are closed and form a circuit that connect a button to ground and the reset pin making the button able to restart the board. As soon as the micro is launched one pin is used to trigger the relay, oppening the reset circuit, making a reset impossible and leaving the button useable for the puzzle. This method has the advantage that the little current drawn by the relay will be when the puzzle is on instead of off (burden is on the less occuring state).

## Puzzles
The following have been implemented:

- Orbix original
- Luminations original (4 corners)
- 4 color map covering
- twisty puzzle

Rejected idea:

- Lumination (12 corners). This add tediousness without much more complexity to the original lumination.

### Orbix original
Thanks to [Jaap's Orbix](https://www.jaapsch.net/puzzles/orbix.htm) puzzle page, I finally found the rules of the original 4 levels of the Orbix (I could remember only the first two correctly). The data provided on the page also contains the numbers of valid positions, simplifying the need for specific backwards scrambles to the minimum.

### Luminations
By considering 4 quadrants of 3 LEDs together, and with the help of the RGB LEDs, the puzzle Luminations can also be replicated. [Jaap's Luminations](https://www.jaapsch.net/puzzles/luminat.htm) puzzle page is, again, a big help for the exact rules.

### 4 color map
This is the classics [4 color map](https://www.chiark.greenend.org.uk/~sgtatham/puzzles/js/map.html) covering problem. This game introduced the necessities to differentiate a clue from a normal LED whit the same color. Showned clues by a breathing pattern animation is investigated.

### Twisty puzzle
With twisty puzzle move where a push on a LED can rotate the status/color of it's neighbor, something close to a [2x2 megaminx](http://www.twistypuzzles.com/cgi-bin/puzzle.cgi?pkey=5723) can be achieved but any led (one-color) is equivalent to a 3-sided corner piece. The goal would be to bring back blocks of the same colors together (so no orientation problem, and no unique-pieces).

# Development
## Pycopy (More up to date and better implementation of micropython)
The process to compile your own pycopy build involve a lot of dependency (cross compiler and such...) Relevant [official doc](https://pycopy.readthedocs.io/en/latest/reference/packages.html).

The `make install`_ take care of setting up your system (debian based asumption), and `make fw-once` will build the tools needed and delete a few unused lib to make room in the memory... `make` will built the firmware *for the specific need of my project*. Have a look at the Makefile if you need more info.

## Flashing the board
`make load` will erase and flash the board. Then use ampy to send/receive files, the uart@115200 or the webrepl. Whatever is on the board will be erased !

Finally `make sync` will send the python code file to the board.

## Code
Each puzzle logic, input/output, and the overall game logic has been split up. This makes it easy to have a terminal emulator where I can prototype a new puzzle easily completely on the pc.

I plan to have the puzzle connect to a wifi (configured by being first an access point), so that the code might update itself when a new release tag is available in this repo.

I have a personal variation of the Neopixel library (mostly to handle hsv and aliases assignment). It is self documented with doctest unitest.
