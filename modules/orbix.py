#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Rules (and docstring) for each levels were taken from Jaap's exhaustive puzzles pages:
# https://www.jaapsch.net/puzzles/orbix.htm

from game import GameLogic
from random import randint, choice


class OrbixLogic(GameLogic):
    N = 12
    nb_level = 4

    def is_complete(self):
        return all(map(bool, self.state))

    def _level1push(self, n):
        """
        If you press any button, the 5 adjacent lights will change;
        switch on if they were off and vice versa.
        """
        for i in self.ADJ[n]:
            self.state[i] ^= 1
        return True

    def _level2push(self, n):
        """
        This type of game play is the same as type 1, except that only lit buttons can be pressed.
        Pressing an unlit button has no effect on the lights.
        """
        if self.state[n]:
            return self._level1push(n)
        return False

    def _level3push(self, n):
        """
        With this type of game play only unlit buttons can be pressed,
        and then all buttons EXCEPT the adjacent ones change.
        Pressing a lit button has no effect on the lights.
        """
        if not self.state[n]:
            for i in range(self.N):
                if i not in self.ADJ[n]:
                    self.state[i] ^= 1
            return True
        return False

    def _level4push(self, n):
        """
        With this type of game play only lit buttons can be pressed.
        If the button opposite the pressed one is on then the effect is as type 1/2,
        i.e. the adjacent 5 lights change. If the button opposite the pressed one is off,
        then all buttons except the pressed one change.
        Pressing an unlit button has no effect.
        """
        if self.state[n]:
            if not self.state[self.OPP[n]]:
                for i in range(self.N):
                    if i != n:
                        self.state[i] ^= 1
                return True
            return self._level1push(n)
        return False

    def scramble(self):
        self.state = [1] * self.N
        while self.is_complete() or sum(self.state) == 0:
            # need at least one on for some level
            if self.level == 2:
                # All state are not reachable for level 2, so we stay in those sub-group
                for _ in range(50):
                    possible = [i for i, state in enumerate(self.state) if state]
                    self._level2push(choice(possible))
            else:
                self.state = [randint(0, 1) for _ in range(self.N)]
                if self.is_complete() and self.level == 4:
                    self.state[randint(0, 11)] = 1

    def execute(self, n, alt=False):
        execute_move = {
            1: self._level1push,
            2: self._level2push,
            3: self._level3push,
            4: self._level4push,
        }
        return execute_move[self.level](n)
