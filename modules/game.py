#!/usr/bin/env python
# -*- coding: utf-8 -*-

from common import orbix_neighbors, orbix_opposites

Actions = {
    "MOVE": "move",
    "MOVE_ALT": "move_alt",
    "RESTART": "restart",
    "QUIT": "quit",
    "NEXT_LEVEL": "next_level",
    "NEXT_GAME": "next_game",
}


def dist(adjacence, cell_a, cell_b):
    if cell_a == cell_b:
        return 0
    if cell_a in adjacence[cell_b]:
        return 1
    if set(adjacence[cell_a]) & set(adjacence[cell_b]):
        return 2
    return 3


def mutual_dists(adjacence, nodes):
    lst = []
    for node_a in nodes:
        cur = []
        for node_b in nodes:
            cur.append(dist(adjacence, node_a, node_b))
        cur.sort()
        lst.append(cur)
    return lst


def read_savegame():
    with open("savegame.sav", "r") as save:
        game_start = int(save.readline())
        level_start = int(save.readline())
        state_current = eval(save.readline())

        return game_start, level_start, state_current


class GameLogic:
    ADJ = orbix_neighbors
    OPP = orbix_opposites

    def __init__(self):
        self.state = [0] * self.N
        self.clues = [False] * self.N
        self.level = 1

    def play(self, level, state=None):
        self.level = min(self.nb_level, max(0, level))
        self.animation_new_game()
        if state is None:
            self.scramble()
        else:
            self.state = state
        start_state = self.state[::]

        while not self.is_complete():
            self.display()
            print(self.state)
            # action, n = self.get_action()
            action, n = self.get_action_test()
            print("action:", action, n)
            if action in [Actions["MOVE"], Actions["MOVE_ALT"]]:
                if not self.execute(n, alt=(action == Actions["MOVE_ALT"])):
                    self.animation_forbiden()
            elif action == Actions["RESTART"]:
                self.state = start_state[::]
            else:
                # Other action terminate the play
                return action

        self.display()  # To see solved state in term mode
        self.animation_win()
        return "win"
