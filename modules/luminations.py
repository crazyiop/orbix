#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Rules (and docstring) for each levels were taken from Jaap's exhaustive puzzles pages:
# https://www.jaapsch.net/puzzles/luminat.htm

from game import GameLogic

from random import randint
from collections import deque


class LuminationsLogic(GameLogic):
    N = 4
    nb_level = 5
    cycle_per_level = {1: 5, 2: 5, 3: 6, 4: 7, 5: 9}

    def __init__(self):
        super().__init__()
        self.previous_move = deque()

    def _step(self, n):
        """
        Perform one evolution step for a particular led
        """
        if n is not None:
            self.state[n] = (self.state[n] + 1) % self.cycle_per_level[self.level]

    def is_complete(self):
        return sum(self.state) == (self.cycle_per_level[self.level] - 1) * self.N

    def _level1push(self, n):
        """
        The lights change through the following cycle of 5 colours:
        green, off, yellow, flashing green, red.
        A move changes only the light of the corner that is brought up.
        """
        self._step(n)
        self.previous_move.popleft()
        self.previous_move.append(n)
        return True

    def _level2push(self, n):
        """
        The lights change through the following cycle of 5 colours:
        flashing yellow, off, green, yellow, red.
        A move changes two lights - the one that is brought up and the one that is brought down.
        """
        self.previous_move.append(n)
        while len(self.previous_move) > 2:
            self.previous_move.popleft()

        for corner in self.previous_move:
            self._step(corner)
        return True

    def _level3push(self, n):
        """
        The lights change through the following cycle of 6 colours:
        green, flashing red, flashing yellow, flashing green, yellow, red.
        A move will change at most one corner light, the one that is brought up.
        It does not change however if that corner was up in either of
        the previous two positions before making that move.
        In other words, if you want to change the colour of the corner that is pointing up,
        you must bring that corner down, and do at least two more moves while keeping
        that corner down before moving it up again.
        """
        if n not in self.previous_move:
            self._step(n)
        self.previous_move.append(n)
        while len(self.previous_move) > 3:
            self.previous_move.popleft()
        return True

    def _level4push(self, n):
        """
        The lights change through the following cycle of 7 colours:
        yellow, off, flashing yellow, green, flashing red, flashing green, red.
        A move changes three lights, namely all except the one that is brought up.
        """
        for corner in set(range(self.N)) - {n}:
            self._step(corner)
        self.previous_move.popleft()
        self.previous_move.append(n)
        return True

    def _level5push(self, n):
        """
        The lights change through the following cycle of 9 colours:
        flashing green, flashing yellow, green, flashing red, off, yellow, green,
        flashing yellow, red.
        Note that green and flashing yellow occur twice, so it is not possible to
        distinguish the exact state of a corner if one of those colours shows.
        Any corner that is moved up will change, and it will also change during the next
        three moves.
        This is additive, so if you move up a corner that has already been up before in the
        last three positions, it will shift twice along the colour cycle.
        Every move therefore changes the lights a total of four cycle shifts - each corner
        shifts as often as that corner has been up in the new position and the previous
        three combined.
        """
        self.previous_move.append(n)
        while len(self.previous_move) > 4:
            self.previous_move.popleft()

        for corner in self.previous_move:
            self._step(corner)

        return True

    def scramble(self):
        self.state = [
            randint(0, self.cycle_per_level[self.level] - 2) for _ in range(self.N)
        ]
        self.previous_move.clear()
        self.previous_move.append(None)

    def execute(self, n, alt=False):
        execute_move = {
            1: self._level1push,
            2: self._level2push,
            3: self._level3push,
            4: self._level4push,
            5: self._level5push,
        }

        # do nothing if a corner is repeated
        last = self.previous_move.popright()
        self.previous_move.append(last)
        if n == last:
            return True

        res = execute_move[self.level](n)
        print(self.previous_move)
        return res
