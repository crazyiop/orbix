#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
Provide high level Pixels implementation:

MagicPixel
----------
 - is somewhat compatible with NeoPixel library: will accept rgb assignment. Color not fully
   identical too
    >>> np = MagicPixel(4, 3, rainbow=False)
    >>> np.show()
    [0, 0, 0, 0, 0, 0, 0, 0, 0]
    >>> np[0] = (100, 200, 255)
    >>> np.show()
    [154, 99, 199, 0, 0, 0, 0, 0, 0]

    Note:
     - 1 rounding error can occur due to math done (stored value are hsv, not rgb).
     - the rgb mode is here for assignment compatibility only. For the rest, hsv mode is assumed.

- allow to assign hue (default S=0.82, V=0.34) or string to a led from:
  ["White", "Cyan" , "Green", "Orange", "Pink" , "Purple", "Red", "Yellow", None]
    >>> np.default_sv(0.6, 0.75) # Used at .show() call
    >>> np[0] = None
    >>> np[1] = 'Red' # Color from the dracula color set available
    >>> np[2] = 0 # Int are taken as the hue in hsv (default S and V used)
    >>> np.show()
    [0, 0, 0, 10, 89, 10, 76, 191, 76]

- A default palette exists (based on Dracula theme) but you can provide your own, expressed in hsv:
    >>> mp = MagicPixel(4, 1, palette={'strongRed': (350, 1, 1), 'paleRed': (350, 0.4, 0.8)})

- use hsv mode for assignment
    >>> mp = MagicPixel(4, 1)
    >>> mp[0] = (156, 0.5, 0.9)
    >>> mp.show()
    [211, 114, 132]

- allow the use of an aliases dictionnary to have custom key for pixel assignment
  (usefull for led cube (or any coordinate system), or simple mnemonic):
    >>> mp = MagicPixel(4, 3, aliases={(0, 0):0, (0, 1):1, 'up':2, 'left':0})
    Traceback (most recent call last):
    ...
    ValueError: Index 0 is used more than once
    >>> mp = MagicPixel(4, 3, aliases={(0, 0):0, (0, 1):1, 'up':2, 'left':0}, \
            allow_surjectif=True)
    >>> mp[1] = 0 # normal indexing still works if not overriden
    >>> mp[(0, 1)] = 'White' # indexing can be used from
    >>> mp['up'] = (156, 0.5, 0.9)
    >>> mp.show()
    [0, 0, 0, 243, 244, 239, 211, 114, 132]
    >>> mp['left'] = 'Green'
    >>> print(mp[(0, 0)])
    (160, 0.88, 0.34)
    >>> print(mp[0])
    (160, 0.88, 0.34)

- Provide an iterator to go through the coordinates (index not guarantee in surjectif mode !)
    >>> list(mp.coords())
    ['left', (0, 1), 'up']
    >>> mp.coord(0)
    'left'

- Complementary helper (doesn't works on rgb tuple):
    >>> mp.complementary(159)
    339
    >>> mp.complementary('Purple')
    450
    >>> mp.complementary((159,0.3,0.8))
    339
"""

try:
    from esp import neopixel_write
    from machine import Pin

except ImportError:
    # for unit doctests
    from unittest.mock import MagicMock

    Pin = MagicMock()

    def neopixel_write(pin, send_buffer, is800khz):
        print([int(byte) for byte in send_buffer])


# original inspiration from https://github.com/dracula/dracula-theme
dracula = {
    "Red": (0, 0.88, 0.35),
    "Orange": (48, 0.85, 0.35),
    "Yellow": (90, 0.81, 0.3),
    "Green": (160, 0.88, 0.34),
    "Cyan": (195, 0.81, 0.35),
    "Purple": (270, 0.79, 0.34),
    "Pink": (318, 0.83, 0.35),
    "White": (60, 0.03, 0.97),
}


def _out_as_byte(value):
    return int(255 * value)


def rgb2hsv_spectrum(r, g, b):
    """
    This allow for rgb assignemnt, we convert into rgb
    this is a plain rgb 2 hsv, but the hsv2rbg use corrected color
    so a little drift may appear
    """
    for c in [r, g, b]:
        assert int(c) == c
        assert c < 256

    r, g, b = r / 255.0, g / 255.0, b / 255.0
    m, _, M = sorted([r, g, b])
    delta = M - m
    if M == m:
        h = 0
    elif M == r:
        h = (60 * ((g - b) / delta) + 360) % 360
    elif M == g:
        h = (60 * ((b - r) / delta) + 120) % 360
    elif M == b:
        h = (60 * ((r - g) / delta) + 240) % 360
    if M == 0:
        s = 0
    else:
        s = delta / M
    v = M
    return int(h), round(s, 2), round(v, 2)


def hsv2rgb_rainbow(h, s, v):
    h = int(h) % 360
    quadrant, offset = divmod(h, 360 // 8)
    third = 1 / 3
    frac = offset * third / (360 // 8)

    if quadrant == 0:
        r, g, b = 1 - frac, frac, 0
    elif quadrant == 1:
        r, g, b = 1 - third, third + frac, 0
    elif quadrant == 2:
        r, g, b = 1 - third - 2 * frac, 2 * third + frac, 0
    elif quadrant == 3:
        r, g, b = 0, 1 - frac, frac
    elif quadrant == 4:
        r, g, b = 0, 1 - third - 2 * frac, third + 2 * frac
    elif quadrant == 5:
        r, g, b = frac, 0, 1 - frac
    elif quadrant == 6:
        r, g, b = third + frac, 0, 1 - third - frac
    elif quadrant == 7:
        r, g, b = 2 * third + frac, 0, 1 - 2 * third - frac

    # print(quadrant, r,g,b)
    # Normalize
    m = v * (1 - s)  # min, max being V
    return tuple(_out_as_byte(i * (v - m) + m) for i in [r, g, b])


class MagicPixel:

    ORDER = (1, 0, 2, 3)  # transmition in g, r ,b (,w) order

    def __init__(
        self,
        pin_number,
        n,
        palette=None,
        aliases=None,
        allow_surjectif=False,
        bpp=3,
        timing=True,
        rainbow=True,
    ):
        self.pin = Pin(pin_number, Pin.OUT)
        self.n = n
        self.bpp = bpp
        self.colors = [(0, 0, 0)] * n
        self.send_buffer = bytearray(n * bpp)
        self.timing = timing
        self.S = 0.82
        self.V = 0.34

        if palette == None:
            self.palette = dracula
        else:
            self.palette = palette

        # Make the get/set map, {set,get}item fallback to identity
        self.set_map = {}
        self.get_map = {}
        if aliases is not None:
            self.set_map = aliases
            for k, v in self.set_map.items():
                if v in self.get_map and not allow_surjectif:
                    raise ValueError("Index {} is used more than once".format(v))
                self.get_map[v] = k

    def __len__(self):
        return self.n

    def __repr__(self):
        return (
            "MagicPixel({pin}, {n}, aliases={aliases}, bpp={bpp}'):\n{colors}"
        ).format(
            pin=self.pin,
            n=self.n,
            aliases=self.set_map,
            bpp=self.bpp,
            colors=self.colors,
        )

    def __setitem__(self, index, val):
        if val is None:
            # turned off by default
            color = (0, 0, 0)
        elif isinstance(val, str):
            if val in self.palette:
                color = self.palette[val]
            else:
                raise ValueError("no color {} in colorset".format(val))
        elif isinstance(val, int):
            # only h given
            color = (val, self.S, self.V)
        elif val[1] <= 1 or val[2] <= 1:
            # hsv given
            color = val
        else:
            # assume rgb given
            color = rgb2hsv_spectrum(*val)

        try:
            self.colors[self.set_map[index]] = color
        except KeyError:
            self.colors[index] = color

    def __getitem__(self, index):
        try:
            return self.colors[self.set_map[index]]
        except KeyError:
            return self.colors[index]

    def coord(self, index):
        return self.get_map[index]

    def default_sv(self, s, v):
        self.S = s
        self.V = v

    def coords(self):
        for i in range(self.n):
            yield self.coord(i)

    def clear(self):
        self.fill((0, 0, 0))

    def fill(self, color):
        for i in range(self.n):
            self[i] = color

    def update(self, colors):
        for i, color in enumerate(colors):
            self.__setitem__(i, color)

    def show(self):
        for index, color in enumerate(self.colors):
            offset = index * self.bpp
            rgb = hsv2rgb_rainbow(*color)
            for i in range(self.bpp):
                self.send_buffer[offset + self.ORDER[i]] = rgb[i]
        neopixel_write(self.pin, self.send_buffer, self.timing)

    def complementary(self, color):
        if isinstance(color, int):
            return color + 180
        elif isinstance(color, tuple):
            return color[0] + 180
        elif isinstance(color, str):
            return self.palette[color][0] + 180
        raise ValueError("wrong color: {}".format(color))


def test(pin_number=2):
    print("displaying MagicPixel hardware test pattern...")
    from time import sleep

    palette = dracula

    n = len(palette)
    mp = MagicPixel(pin_number, n, palette=palette)
    for _ in range(n):
        for offset in range(n):
            for i, c in enumerate(palette):
                mp[(i + offset) % n] = c
            mp.show()
            sleep(0.1)


if __name__ == "__main__":
    # self test
    import doctest

    print(doctest.testmod())
