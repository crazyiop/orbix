#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from game import GameLogic, mutual_dists
from random import randint, choice

"""
Custom puzzle that works like a twisty puzzle.
A move will rotate by one step the 5 LEDs that surround the button pressed.
A long press will rotate one step backward.

The goal is to put back the colors together as group. A group should be as compact as possible:
    - 3 leds should be in a triangle shape
    - 4 leds should be in a losange shape
    - 6 leds should form a complete half

The last level is a bandage one. One cell is dead and cannot move (restricting half of the moves)
In this level, the colors should be put into concentric circles.
"""


class TwistyLogic(GameLogic):
    N = 12
    nb_level = 4

    def _is_half(self, nodes):
        return len(nodes) == self.N // 2 and any(
            set(nodes) == set([el] + self.ADJ[el]) for el in nodes
        )

    def _is_third(self, nodes):
        if len(nodes) != self.N // 3:
            return False
        dists = mutual_dists(self.ADJ, nodes)
        dists.sort()
        return dists == [[0, 1, 1, 1], [0, 1, 1, 1], [0, 1, 1, 2], [0, 1, 1, 2]]

    def _is_quarter(self, nodes):
        return len(nodes) == self.N // 4 and all(
            dists == [0, 1, 1] for dists in mutual_dists(self.ADJ, nodes)
        )

    def _is_concentric(self, nodes):
        # Assume in respect to node 0/11 centers
        return nodes in [[0], [1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11]]

    def is_complete(self):
        complete_part = [
            self._is_half,
            self._is_third,
            self._is_quarter,
            self._is_concentric,
        ]
        parts = {i: [] for i in range(self.level + 1)}
        for node in range(self.N):
            parts[self.state[node]].append(node)
        # parts are cells numbers grouped by same color
        return all(complete_part[self.level - 1](part) for part in parts.values())

    def scramble(self):
        """Scramble a twisty puzzle."""
        # Even if partiy should not be a problem because of mulitplicity of same-color node
        # the scrambles from those solved states ensure a solution
        start = 0
        if self.level == 1:
            self.state = [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1]
        elif self.level == 2:
            self.state = [0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2]
        elif self.level == 3:
            self.state = [0, 0, 0, 1, 1, 2, 2, 2, 1, 3, 3, 3]
        elif self.level == 4:
            self.state = [4, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3]
            start = 6

        while self.is_complete():
            for _ in range(25):
                n = randint(start, self.N - 1)
                alt = choice([True, False])
                self.execute(n, alt)

    def execute(self, n, alt=False):
        """Rotate the state of adjacent cells of n.

        Alt allow to control he rotation way
        """
        if self.level == 4 and n <= 5:
            return False

        if not alt:
            turn = 1
        else:
            turn = 4  # Lazy coding

        for _ in range(turn):
            # Clockwise turn
            tmp = self.state[self.ADJ[n][0]]
            for current, following in zip(self.ADJ[n], self.ADJ[n][1:]):
                self.state[current] = self.state[following]
            self.state[self.ADJ[n][-1]] = tmp
        return True
