#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Custom implementation of the game:
# https://www.chiark.greenend.org.uk/~sgtatham/puzzles/js/map.html

from game import GameLogic
from random import shuffle, randint


class MapLogic(GameLogic):
    REDUNDANT_HINT_PER_LEVEL = {1: 3, 2: 2, 3: 1, 4: 0}
    nb_level = 4
    N = 12

    def is_complete(self):
        error = False
        for i in range(self.N):
            for nb in self.ADJ[i]:
                if (
                    self.state[i] != 0
                    and self.state[nb] != 0
                    and self.state[i] == self.state[nb]
                ):
                    print(i, "==", nb)
                    error = True

        if error:
            return False
        if any(c == 0 for c in self.state):
            return False
        return True

    def _push(self, n):
        if self.clues[n]:
            return False
        self.state[n] = (self.state[n] + 1) % 5
        return True

    def solve(self, start):
        to_visit = [start]
        solution_node = None
        nb_solution = 0
        while to_visit and nb_solution <= 1:
            node = to_visit.pop()
            used = dict()
            for i, nb in enumerate(self.ADJ):
                if node[i] == 0:
                    used[i] = set(node[j] for j in nb) - {0}

            if not used:
                # all region filled
                solution_node = node
                nb_solution += 1
                continue
            if any(len(v) == 4 for v in used.values()):
                # one region has no possible choice
                continue

            m = min(4 - len(used[k]) for k in used.keys())
            cells = [k for k, v in used.items() if 4 - len(v) == m]
            shuffle(cells)
            pivot = cells[randint(0, len(cells) - 1)]
            possibilities = list({1, 2, 3, 4} - used[pivot])
            shuffle(possibilities)
            for possibility in possibilities:
                new = node[::]
                new[pivot] = possibility
                to_visit.append(new)

        return solution_node, nb_solution

    def make_puzzle(self, solution):
        """Trim a solutions of hints, keeping solvability."""
        to_determined = list(range(12))
        shuffle(to_determined)
        clues = [False] * self.N
        initial_state = solution[::]
        for cell in to_determined:
            initial_state[cell] = 0
            _, nb_solution = self.solve(initial_state)
            if nb_solution != 1:
                clues[cell] = True
                initial_state[cell] = solution[cell]

        for hint in range(self.REDUNDANT_HINT_PER_LEVEL[self.level]):
            # From a least number of hint, add some to make different level
            i = initial_state.index(0)  # First non-clue cell
            initial_state[i] = solution[i]
            clues[i] = True
        self.state = initial_state
        self.clues = clues

    def scramble(self):
        pattern, _ = self.solve([0] * self.N)
        self.make_puzzle(pattern)

    def execute(self, n, alt=False):
        return self._push(n)
