from time import sleep
from magicpixel import MagicPixel
from common import orbix_neighbors, orbix_opposites

from random import randint

try:
    # from machine import Timer
    import micropython

    # allow to get error that occurs in ISR
    micropython.alloc_emergency_exception_buf(100)

except ModuleNotFoundError:
    pass


class OrbixPixel(MagicPixel):
    # breathing curve = normal_law(20) # fixed and precomputed below
    curve = [0, 2, 4, 10, 19, 33, 51, 71, 88, 98, 98, 88, 71, 51, 33, 19, 10, 4, 2, 0]

    def __init__(self, pin_number):
        super().__init__(pin_number, 12)
        self.clues = [False] * self.n

    def __repr__(self):
        return "OrbixPixel(pin_number={pin}): {colors}".format(
            pin=self.pin, colors=self.colors
        )

    """
    def _timer_run(self):
        print("launching timer")
        self.timer.init(
            mode=Timer.PERIODIC,
            period=1800 // len(sinusoid),
            callback=self._periodic_update,
        )

    def _periodic_update(self, timer_ref):
        print("in periodic update")
        for i, (color, is_clue) in enumerate(zip(self.colors, self.clues)):
            if is_clue:
                # heartbeat
                # self.np[i] = hsv2rgb(hue, _S, _V * sinusoid[self.tick] / 100)
                pass
            else:
                if color < 0:
                    # off
                    self.np[i] = hsv2rgb(0, 0, 0)
                else:
                    pass
                    # still
                    # self.np[i] = hsv2rgb(hue, _S, _V)
        self.tick = (self.tick + 1) % len(sinusoid)
        # self.np.show()
    """

    def update(self, colors, clues=None):
        super().update(colors)

        previous = self.colors[:]
        if clues is None:
            self.clues = [False] * self.n
        else:
            self.clues = clues

        new_colors = []

        if clues:
            for color, clue in zip(self.colors, self.clues):
                h, s, v = color
                new_colors.append((h, s, v / 2))
            super().update(new_colors)
        self.show()
        return previous

    def _get_wave_layers(self, start, adj):
        """
        >>> led = OrbixPixel(4)
        >>> led._get_wave_layers(0, [[1, 2, 3, 5, 6], [0, 2], [1, 0], [0, 4], \
            [3, 7], [4], [0, 7], [4, 6, 8], [7]])
        [{0}, {1, 2, 3, 5, 6}, {4, 7}, {8}]
        """
        used = set()
        current = {start}
        layers = []

        while current:
            layers.append(current)
            used.update(current)
            new = set()
            for cell in current:
                new.update(adj[cell])
            current = new - used
        return layers

    def fade_out(self):
        colors = self.colors[:]
        for fade in self.curve[len(self.curve) // 2 :]:
            self.update([(h, s, v * fade / 100) for (h, s, v) in colors])
            sleep(0)

    def expand_wave(self, color, start=None, p=4, delay=0):
        if isinstance(color, str):
            hue = self.palette[color][0]
        else:
            hue = color

        if start is None:
            start = randint(0, 11)
        layers = self._get_wave_layers(start, orbix_neighbors)
        shift = len(self.curve) // p

        stop = len(self.curve) + len(layers) * shift
        for frame in range(stop):
            colors = [None] * self.n
            for li, l in enumerate(layers):
                index = frame - shift * li
                if index >= 0 and index < len(self.curve):
                    v = self.curve[frame - shift * li] * self.V / 100
                else:
                    v = 0
                for cell in l:
                    colors[cell] = (hue, self.S, v)
            self.update(colors)
            sleep(delay)

    def blink(self, color, delay):
        backup = self.update([color] * self.n)
        sleep(delay)
        self.colors = backup

    # Animation callback needed for the game:

    def win(self):
        self.expand_wave("Green", 0)
        self.expand_wave("Green", 11)

    def forbiden(self):
        self.blink("Red", 0.05)
        self.blink(None, 0.05)
        self.blink("Red", 0.05)

    def start_game(self, level, color):
        cell = range(level)
        for cell in range(level):
            self[cell] = color
            self[orbix_opposites[cell]] = self.complementary(color)
        sleep(1)
        self.clear()


def test():
    led = OrbixPixel(2)
    n = len(led)
    while True:
        led.expand_wave("Purple", 0)
        for _ in range(20):
            colors = [randint(0, 359) for _ in range(n)]
            led.update(colors)
            sleep(0.1)

        for i in range(n):
            colors = [None] * n
            colors[i] = 226
            led.update(colors)
            sleep(0.3)


if __name__ == "__main__":
    # self test
    import doctest

    print(doctest.testmod())
