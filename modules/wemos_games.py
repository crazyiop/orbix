#!/usr/bin/env python
# -*- coding: utf-8 -*-

from random import choice
from orbixpixel import dracula

from game import Actions, dist
from orbix import OrbixLogic
from twisty import TwistyLogic
from luminations import LuminationsLogic
from map4 import MapLogic


class WemosGame:
    def __init__(self, leds, buttons):
        self.leds = leds
        self.buttons = buttons

    def get_action(self):
        pressed, length = self.buttons.scan()
        if length == "timeout":
            self.leds.expand_wave("Red")
            return "quit", None

        if len(pressed) == 1:
            if length == "short":
                return "move", pressed[0]
            else:
                return "move_alt", pressed[0]
        if len(pressed) == 2 and length == "long":
            if dist(*pressed) == 1:
                return "next_level", None
            if dist(*pressed) == 2:
                return "next_game", None
            if dist(*pressed) == 3:
                # power off
                self.leds.expand_wave("Red", pressed[0])
                return "quit", None

    def get_action_test(self):
        action = ""
        while (
            not (action.isdigit() and int(action) in range(self.N))
            and action not in Actions.values()
        ):
            action = input(
                "next move? ('-n' for 'alt' move) or {}\n".format(Actions.values())
            )
            if not action:
                continue

            if action[0] == "-":
                res = "move_alt"
                action = action[1:]
            else:
                res = "move"

        try:
            return res, int(action)
        except ValueError:
            return action, None

    def display(self):
        colors = [self.colorset[led] for led in self.state]
        self.leds.update(colors, self.clues)

    def animation_win(self):
        print("You win !")
        self.leds.win()
        input("press a key to start a new game")

    def animation_forbiden(self):
        print("forbiden")
        self.leds.forbiden()

    def animation_new_game(self):
        print("New game. level:", self.level)
        self.leds.start_game(self.level, self.game_color)


class WemosOrbix(WemosGame, OrbixLogic):
    def __init__(self, leds, buttons):
        led_color = "Red"
        while led_color == "Red":
            led_color = choice(list(dracula.values()))
        self.colorset = [None, led_color]
        self.game_color = "Purple"
        WemosGame.__init__(self, leds, buttons)
        OrbixLogic.__init__(self)


class WemosMap(WemosGame, MapLogic):
    def __init__(self, leds, buttons):
        self.colorset = [None, "Orange", "Pink", "Cyan", "Green"]
        self.game_color = "Green"
        WemosGame.__init__(self, leds, buttons)
        MapLogic.__init__(self)


class WemosTwisty(WemosGame, TwistyLogic):
    def __init__(self, leds, buttons):
        self.colorset = ["Red", "Yellow", "White", "Cyan", None]
        self.game_color = "Cyan"
        WemosGame.__init__(self, leds, buttons)
        TwistyLogic.__init__(self)


class WemosLuminations(WemosGame, LuminationsLogic):
    def __init__(self, leds, buttons):
        self.colorset = list(dracula.keys())
        self.game_color = "Orange"
        WemosGame.__init__(self, leds, buttons)
        LuminationsLogic.__init__(self)

    def display(self):
        mapping = [[0, 1, 2], [3, 4, 8], [5, 6, 7], [9, 10, 11]]
        colors = [None] * 12
        for triangle, color in enumerate(self.state):
            for cell in mapping[triangle]:
                colors[cell] = self.colorset[-self.cycle_per_level[self.level] :][color]
        self.leds.update(colors)
