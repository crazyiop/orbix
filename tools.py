import math


def normal_law(nb):
    """
    Get nb values of a sampling of the normal curve for LED breathing effect.
    return a list, normalized to 100
    """

    def f(x):
        return math.exp(-x ** 2 / 2) / math.sqrt(2 * math.pi)

    """
    low = 0
    high = (50)

    mid = (high + low) / 2

    while abs(int(100 * f(high) / f(0)) - int(100 * f(low) / f(0))) > 1:
        if int(100 * f(mid) / f(0)) < 1:
            high = mid
        else:
            low = mid
        mid = (high + low) / 2

    limit = high
    print(limit)
    """
    limit = 3.125  # found by the above, but can be hardcoded as always the same

    step = 2 * limit / (nb - 1)
    return [int(100 * f(-limit + i * step) / f(0)) for i in range(nb)]
