from orbixpixel import OrbixPixel
import gc


def test():
    leds = OrbixPixel(2)
    leds.expand_wave("Cyan")

    from main import WemosOrbix

    game = WemosOrbix(leds, {})
    game.play(1)
    game = None
    gc.collect()

    from main import WemosTwisty

    game = WemosTwisty(leds, {})
    game.play(1)
    game = None
    gc.collect()

    from main import WemosLuminations

    game = WemosLuminations(leds, {})
    game.play(1)
    game = None
    gc.collect()

    from main import WemosMap

    game = WemosMap(leds, {})
    game.play(1)
    game = None
    gc.collect()
