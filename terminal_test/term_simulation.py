from sys import exit

from orbix import OrbixLogic
from map4 import MapLogic
from luminations import LuminationsLogic
from twisty import TwistyLogic

from game import Actions, read_savegame


Colors = {
    "HEADER": "\033[95m",
    "OKBLUE": "\033[94m",
    "OKGREEN": "\033[92m",
    "WARNING": "\033[93m",
    "FAIL": "\033[91m",
    "ENDC": "\033[0m",
    "BOLD": "\033[1m",
    "UNDERLINE": "\033[4m",
}


class TermGame:
    MAP = """
    ┌───────────────┐
    │               │
    │ ┌─────┬─────┐ │
    │ │ {10}10{12}  │  {9}9{12}  │ │
    │ │ ┌─┬─┴─┬─┐ │ │
    │ │ │ │ {2}2{12} │ │ │ │
    │ ├─┤{1}1{12}├───┤{3}3{12}├─┤ │
    │ │ │ │ {0}0{12} │ │ │ │
    │ │ ├─┴─┬─┴─┤ │ │
    │ │{6}6{12}│ {5}5{12} │ {4}4{12} │{8}8{12}│ │
    │ │ ├───┴───┤ │ │
    │ │ │   {7}7{12}   │ │ │
    │ └─┴───────┴─┘ │
    │       {11}11{12}      │
    └───────────────┘
    """

    def get_action(self):
        action = ""
        while (
            not (action.isdigit() and int(action) in range(self.N))
            and action not in Actions.values()
        ):
            action = input(
                f"next move? (int in [0, {self.N - 1}] ('-n' for 'alt' move) range "
                f"or {Actions.values()}\n"
            )
            if not action:
                continue

            if action[0] == "-":
                res = "move_alt"
                action = action[1:]
            else:
                res = "move"

        try:
            return res, int(action)
        except ValueError:
            return action, None

    def get_action_test(self):
        # use by embedded during dev time by common code...
        return self.get_action()

    def display(self):
        bold = [Colors["BOLD"] if clue else "" for clue in self.clues]
        for i, led in enumerate(self.state):
            colors = [self.COLORSET[led] + bold[i] for i, led in enumerate(self.state)]
            print(f"{i:>2} {self.COLORSET[led]}{led}{Colors['ENDC']}")
        print(self.MAP.format(*colors, Colors["ENDC"]))

    def animation_win(self):
        print(f"\t\t{Colors['OKGREEN']}You win !{Colors['ENDC']}")
        input("press a key to start a new game")

    def animation_forbiden(self):
        print(f"\t\t{Colors['FAIL']}forbiden{Colors['ENDC']}")

    def animation_new_game(self):
        print(f"\t\t{Colors['OKBLUE']}New game. (level {self.level}){Colors['ENDC']}")


class TermOrbix(TermGame, OrbixLogic):
    COLORSET = [Colors["FAIL"], Colors["OKGREEN"]]


class TermMap(TermGame, MapLogic):
    COLORSET = [
        "",
        Colors["HEADER"],
        Colors["OKGREEN"],
        Colors["OKBLUE"],
        Colors["FAIL"],
    ]


class TermTwisty(TermGame, TwistyLogic):
    """
    >>> t = TermTwisty()
    >>> t._is_half([4,5,6,7,8,11])
    True
    >>> t._is_half([0,1,2,3,9,10])
    True
    >>> t._is_half([0,1,2,3,7,10])
    False
    >>> t._is_third([0,3,4,8])
    True
    >>> t._is_third([0,3,4,2])
    True
    >>> t._is_third([10,3,4,2])
    False
    >>> t._is_quarter([0,4,5])
    True
    >>> t._is_quarter([0,4,3])
    True
    >>> t._is_quarter([0,4,8])
    False
    """

    COLORSET = [
        Colors["HEADER"],
        Colors["OKGREEN"],
        Colors["OKBLUE"],
        Colors["FAIL"],
        Colors["ENDC"],
    ]


class TermLuminations(TermGame, LuminationsLogic):
    PYRAMID = """
           {3}3{4}
         / | \\
        /  |  \\
       ,   {0}0{4}   `
       | /   \\ |
       {1}1{4} ----- {2}2{4}

    """

    def display(self):
        base_colors = [f"\033[9{i}m" for i in range(9)]
        # place dark to be used last
        base_colors.append(base_colors.pop(0))
        for i, led in enumerate(self.state):
            colors = [base_colors[led] for led in self.state]
            print(f"{i:>2} {base_colors[led]}{led}{Colors['ENDC']}")
        print(self.PYRAMID.format(*colors, Colors["ENDC"]))


if __name__ == "__main__":
    import doctest

    print(doctest.testmod())

    if not doctest.testmod().failed:
        constructors = [TermMap, TermOrbix, TermTwisty, TermLuminations, TermMap]
        try:
            game_start, level_start, state = read_savegame()
        except FileNotFoundError:
            game_start, level_start, state = 0, 1, None
        while True:
            for i in range(game_start, len(constructors)):
                game_start = 0
                game = constructors[i]()

                level = level_start - 1
                level_start = 1
                while True:
                    level += 1
                    action = game.play(level, state)
                    state = None
                    if action == Actions["NEXT_GAME"]:
                        break
                    elif action == Actions["QUIT"]:
                        print("Goodbye !")
                        with open("savegame.sav", "w") as save:
                            save.write(str(i) + "\n")
                            save.write(str(level) + "\n")
                            save.write(str(game.state))
                        exit()
                    elif action == Actions["NEXT_LEVEL"]:
                        pass
                    if action == "win":
                        pass
