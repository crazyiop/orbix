# This file is executed on every boot (including wake-boot from deepsleep)
# import esp
# esp.osdebug(None)
from machine import Pin, reset_cause, DEEPSLEEP_RESET, unique_id
import gc
from main import main


print("\n\n")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("      ORgBIX by Crazyiop")
print("https://gitlab.com/crazyiop/orbix")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
print("Running on target id: {}".format(unique_id()))

# webrepl.start()
if reset_cause() == DEEPSLEEP_RESET:
    print("Woke from a deep sleep")
else:
    print("Power on or hard reset")

gc.collect()


hold = Pin(15, Pin.OUT)
hold.on()

# main()
