from time import sleep, ticks_us, ticks_diff
from machine import Pin


def _get_pressed(state, th, mapping):
    if not mapping:
        return [coord for coord, cell in state.items() if cell > th * 0.75]
    return [mapping[coord] for coord, cell in state.items() if cell > th * 0.75]


class ButtonsMatrix:
    def __init__(self, col_out, row_in, mapping=None):
        # in/out in regards to the wemos
        # as the 'out' momentary value, current flow from row to col when pressed
        # diodes therefore points to the col_out lines
        self.pin_out = [Pin(p, Pin.OUT) for p in col_out]
        self.mapping = mapping

        self.pin_in = []
        for p in row_in:
            try:
                self.pin_in.append(Pin(p, Pin.IN, Pin.PULL_UP))
            except ValueError:
                print("Make sure you added an external pull-up on pin 16 (D0)")
                self.pin_in.append(Pin(p, Pin.IN))
        print("diodes should be directed to column:")
        print("column ", self.pin_out, "-|<|- row", self.pin_in)
        self.state = {}

        # Might need to be adapted on matrix size, if no reactif enough...
        self.time_to_trigger = len(self.pin_in) * len(self.pin_out) / 600

        self.step = 0.001

    def scan(self):
        for pin in self.pin_out:
            pin.on()

        for y in range(len(self.pin_in)):
            for x in range(len(self.pin_out)):
                self.state[(x, y)] = 0

        # Wait for the button to be released before triggering another action
        pressed = True
        while pressed:
            pressed = False
            for x, col in enumerate(self.pin_out):
                col.off()
                for y, row in enumerate(self.pin_in):
                    if not row.value():
                        pressed = True
                col.on()
                sleep(self.step)

        # for _ in range(10000):
        while True:
            for x, col in enumerate(self.pin_out):
                col.off()
                for y, row in enumerate(self.pin_in):
                    if not row.value():
                        self.state[(x, y)] += 1
                        if self.state[(x, y)] > (self.time_to_trigger / self.step) * 5:
                            return (
                                _get_pressed(
                                    self.state, self.state[(x, y)], self.mapping
                                ),
                                "long",
                            )
                    else:
                        if self.state[(x, y)] > self.time_to_trigger / self.step:
                            return (
                                _get_pressed(
                                    self.state, self.state[(x, y)], self.mapping
                                ),
                                "short",
                            )
                        else:
                            # Debounce
                            self.state[(x, y)] = 0
                col.on()
                sleep(self.step)
        return self.state, "timeout"


class ButtonsCharlie:
    def __init__(self, pins, mapping=None):
        self.pins = pins
        self.mapping = mapping

        self.N = len(pins) * (len(pins) - 1)
        if 16 in self.pins:
            # pin 16 does not support pull up, add it externaly
            print("Make sure you added an external pull-up on pin 16 (D0)")

        self.state = {}

        # Might need to be adapted on matrix size, if no reactif enough...
        self.time_to_trigger = self.N / 600
        self.step = 0.001

    def scan(self, timeout=None):
        for pin in self.pins:
            p = Pin(pin, Pin.OPEN_DRAIN)
            p.on()

        self.state = {
            (x, y): 0 for x in range(len(self.pins)) for y in range(len(self.pins))
        }

        # for _ in range(10000):
        wait_for_release = True
        start = ticks_us()

        # while ( ticks_diff(ticks_us(), start) < 5 * 60 * 1000):  # TODO verify againt ticks_max
        while True:
            pressed = False
            for x, out in enumerate(self.pins):
                p = Pin(out, Pin.OUT)
                p.off()

                for y, sense in enumerate(self.pins):
                    if out == sense:
                        continue
                    try:
                        s = Pin(sense, Pin.IN, Pin.PULL_UP)
                    except ValueError:
                        s = Pin(sense, Pin.IN)
                    sleep(self.step)
                    if not s.value():
                        pressed = True
                        if not wait_for_release:
                            # Wait for the button to be released before triggering another action
                            self.state[(x, y)] += 1
                            if (
                                self.state[(x, y)]
                                > (self.time_to_trigger / self.step) * 5
                            ):
                                return (
                                    _get_pressed(
                                        self.state, self.state[(x, y)], self.mapping
                                    ),
                                    "long",
                                )
                    else:
                        if self.state[(x, y)] > self.time_to_trigger / self.step:
                            return (
                                _get_pressed(
                                    self.state, self.state[(x, y)], self.mapping
                                ),
                                "short",
                            )
                        else:
                            # Debounce
                            self.state[(x, y)] = 0
                    s = Pin(sense, Pin.OPEN_DRAIN)
                p.on()
                p = Pin(out, Pin.OPEN_DRAIN)

            if not pressed:
                wait_for_release = False

        return self.state, "timeout"


# Meta Button class
class Buttons:
    def __init__(self, pins, mapping=None):
        if isinstance(pins[0], list):
            if len(pins) != 2:
                raise ValueError("Button a list or a list of two list as pins")
            self.buttons = ButtonsMatrix(
                col_out=pins[0], row_in=pins[1], mapping=mapping
            )
        else:
            self.buttons = ButtonsCharlie(pins=pins, mapping=mapping)

    def scan(self):
        return self.buttons.scan()


def test(pins=[0, 12, 14, 16]):
    buttons = Buttons(pins)
    while True:
        pressed, lenght = buttons.scan()
        print(pressed, lenght)


def associate_led(pins=[0, 12, 14, 16]):
    import orbixpixel

    leds = orbixpixel.OrbixPixel(2)
    buttons = Buttons(pins)
    n = len(leds)
    for i in range(n):
        colors = [None] * n
        colors[i] = "White"
        leds.update(colors)
        print("led {}:".format(i), end="")
        pressed, lenght = buttons.scan()
        print(pressed, lenght)
