export PATH := esp-open-sdk/xtensa-lx106-elf/bin:$(PATH)

all:
	cp -r modules pycopy/ports/esp8266
	echo 'building pycopy'
	make clean -C pycopy/ports/esp8266
	make -C pycopy/ports/esp8266


install:
	sudo apt install gperf bison flex texinfo help2man libtool-bin libncurses-dev libffi-dev
	git submodule update --init --recursive
	pip install --user esptool


fw-once:
	echo 'building cross compiler'
	cd esp-open-sdk
	make
	cd ..
	echo 'building pycopy dependency'
	make -C pycopy/mpy-cross
	make -C pycopy/ports/unix
	make -C pycopy/ports/esp8266
	echo 'setting up custom lib'
	pycopy/ports/unix/pycopy -m upip install -p pycopy/ports/esp8266/modules pycopy-itertools
	pycopy/ports/unix/pycopy -m upip install -p pycopy/ports/esp8266/modules pycopy-random
	pycopy/ports/unix/pycopy -m upip install -p pycopy/ports/esp8266/modules pycopy-collections
	pycopy/ports/unix/pycopy -m upip install -p pycopy/ports/esp8266/modules pycopy-collections.deque
	rm -f pycopy/ports/esp8266/modules/neopixel.py
	rm -f pycopy/ports/esp8266/modules/apa102.py
	rm -f pycopy/ports/esp8266/modules/dht.py
	rm -f pycopy/ports/esp8266/modules/ds18x20.py
	rm -f pycopy/ports/esp8266/modules/onewire.py


load:
	esptool.py --port /dev/ttyUSB0 erase_flash
	esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect -fm dio 0 pycopy/ports/esp8266/build/firmware-combined.bin


sync:
	ampy -p /dev/ttyUSB0 put boot.py
	ampy -p /dev/ttyUSB0 put button.py
	#ampy -p /dev/ttyUSB0 put common.py
	#ampy -p /dev/ttyUSB0 put game.py
	ampy -p /dev/ttyUSB0 put main.py
	ampy -p /dev/ttyUSB0 put mpu6050.py
	ampy -p /dev/ttyUSB0 put testgame.py

test:
	python3 modules/magicpixel.py
	python3 modules/orbixpixel.py

flake:
	flake8 *.py modules/*.py --ignore=D,W503,E203

clean:
	make -C pycopy/ports/esp8266 clean
	rm -f pycopy/ports/esp8266/modules/luminations.py
	rm -f pycopy/ports/esp8266/modules/magicpixel.py
	rm -f pycopy/ports/esp8266/modules/map4.py
	rm -f pycopy/ports/esp8266/modules/orbixpixel.py
	rm -f pycopy/ports/esp8266/modules/orbix.py
	rm -f pycopy/ports/esp8266/modules/random.py
	rm -f pycopy/ports/esp8266/modules/twisty.py
	rm -f pycopy/ports/esp8266/modules/wemos_games.py
