from machine import I2C, Pin
from time import sleep


class Accelero:
    def __init__(self, scl_pin=5, sda_pin=4, addr=0x68):
        self.i2c = I2C(scl=Pin(scl_pin), sda=Pin(sda_pin))
        self.addr = addr
        self.data = []

        # reinit all signal path
        self.i2c.start()
        self.i2c.writeto(self.addr, bytearray([104, 0x07]))
        self.i2c.stop()

        # clear interrupt
        self.clear_int()

        # take out of sleep mode
        self.i2c.start()
        self.i2c.writeto(self.addr, bytearray([107, 0]))
        self.i2c.stop()

        # Set motion detect threshold
        self.i2c.start()
        self.i2c.writeto(self.addr, bytearray([31, 20, 40]))
        # 20 for the threshold
        # 40 for the duration. (LSB is 1 ms @ 1 kHz rate)
        self.i2c.stop()

        # Set the high pass filter
        self.i2c.start()
        self.i2c.writeto(self.addr, bytearray([28, 0x01]))
        # Set the Digital High Pass Filter, bits 3:0. 0x01 for 5Hz. (0, filter always output 0)
        # These 3 are apparently used even if they are not documented...
        self.i2c.stop()

    def motion_detect(self, enable):
        self.i2c.start()
        if enable:
            self.i2c.writeto(self.addr, bytearray([55, 0xA0, 0x40]))
        else:
            # disable all interrupt
            self.i2c.writeto(self.addr, bytearray([56, 0x00]))
        self.i2c.stop()

    def clear_int(self):
        return self.i2c.readfrom_mem(self.addr, 58, 1)

    def get_raw_values(self, addr=0x3B, n=14):
        self.i2c.start()
        acc_status = self.i2c.readfrom_mem(self.addr, addr, n)
        self.i2c.stop()
        return acc_status

    def get_rand(self):
        data = self.get_raw_values()
        res = 0
        for i, byte in enumerate(data):
            res ^= byte * 256 ** (i % 2)
        return res

    def update(self):
        self.data = self.get_raw_values()

    @staticmethod
    def bytes_toint(data):
        firstbyte, secondbyte = data
        if not firstbyte & 0x80:
            return firstbyte << 8 | secondbyte
        return -(((firstbyte ^ 255) << 8) | (secondbyte ^ 255) + 1)

    def get_values(self):
        raw_ints = self.get_raw_values()
        vals = {}
        vals["AcX"] = self.bytes_toint(raw_ints[0:2])
        vals["AcY"] = self.bytes_toint(raw_ints[2:4])
        vals["AcZ"] = self.bytes_toint(raw_ints[4:6])
        vals["Tmp"] = self.bytes_toint(raw_ints[6:8]) / 340.00 + 36.53
        vals["GyX"] = self.bytes_toint(raw_ints[8:10])
        vals["GyY"] = self.bytes_toint(raw_ints[10:12])
        vals["GyZ"] = self.bytes_toint(raw_ints[12:14])
        return vals  # returned in range of Int16 -32768 to 32767

    def shake_detect(self, callback, wemos_pin):
        self.motion_detect(enable=True)
        pint = Pin(wemos_pin, Pin.IN, Pin.PULL_UP)
        pint.irq(trigger=Pin.IRQ_FALLING, handler=callback)


def test_shake_detect():
    acc = Accelero()

    def callback(pin_triggered):
        print("\nmotion detected")
        acc.clear_int()
        sleep(0.1)

    acc.shake_detect(callback)
    while True:
        print(".", end="")
        sleep(3)


def test():
    acc = Accelero()
    seed = acc.get_rand()
    print("random seed:", seed)
    while True:
        print(acc.get_values())
        sleep(0.5)
